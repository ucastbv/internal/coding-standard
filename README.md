# UCast Coding Standard

The UCast Coding standard is a set of [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) rules applied to all UCast projects.

## Usage

Installation can be done with [Composer](https://getcomposer.org/), by requiring this package as a development dependency:

```bash
composer require --dev ucast/coding-standard
```

After that, use the coding standard in your [PHPCS](https://github.com/squizlabs/PHP_CodeSniffer) command:

```bash
phpcs --standard=UCast /path/to/some/file/to/sniff.php
```

That's it!
